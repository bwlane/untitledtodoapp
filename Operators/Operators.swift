//
//  Operators.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

import Foundation

// MARK: - Forward Application

precedencegroup ForwardApplication {
    associativity: left
}

infix operator |>: ForwardApplication

func |> <A, B>(a: A, f: (A) -> B) -> B {
    f(a)
}

// MARK: - Forward Composition (Semigroupoid)

precedencegroup ForwardComposition {
    associativity: right
    higherThan: ForwardApplication
}

infix operator >>>: ForwardComposition

func >>> <A, B, C>(f: @escaping (A) -> B, g: @escaping (B) -> C) -> ((A) -> C) {
    { a in g(f(a)) }
}

// MARK: - Semigroup Composition

precedencegroup SemigroupComposition {
    associativity: right
    higherThan: ForwardApplication
    lowerThan: ComparisonPrecedence
}

infix operator <>: SemigroupComposition

func <> <A>(f: @escaping (A) -> Void, g: @escaping (A) -> Void) -> (A) -> Void {
    { a in
        f(a)
        g(a)
    }
}
