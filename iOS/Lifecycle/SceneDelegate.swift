//
//  SceneDelegate.swift
//  UntitledToDoApp (iOS)
//
//  Created by Brian Lane on 6/1/21.
//

import SwiftUI
#if os(macOS)
import Cocoa
#elseif os(iOS)
import UIKit
#endif

import ComposableArchitecture

// TODO: Compare with TicTacToe app and see what can be shared
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        self.window = (scene as? UIWindowScene).map(UIWindow.init(windowScene:))
        
//        let rootView = AppView(
//            Store(
//                initialState: AppState(todos: .mock),
//                reducer: appReducer,
//                environment: AppEnvironment(
//                    mainQueue: .main,
//                    uuid: UUID.init
//                )
//            )
//        )
        
        let navController = UINavigationController()
        let rootView = MainViewController(Store(
                                                initialState: AppState(todos: .mock),
                                                reducer: appReducer,
                                                environment: AppEnvironment(
                                                    mainQueue: .main,
                                                    uuid: UUID.init
                                                )
                                            )
        )
        
        navController.pushViewController(rootView, animated: false)
        
        self.window?.rootViewController = navController
        self.window?.makeKeyAndVisible()
    }
}

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
    
    // MARK: - UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}

