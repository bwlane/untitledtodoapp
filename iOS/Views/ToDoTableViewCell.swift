//
//  ToDoTableViewCell.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {
    
}

extension ToDoTableViewCell: Reusable {
    static func reuseIdentifier() -> String {
        "ToDoTableViewCell"
    }
}
