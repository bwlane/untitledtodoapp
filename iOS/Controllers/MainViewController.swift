//
//  MainViewController.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/5/21.
//

import ComposableArchitecture
import UIKit

class MainViewController: UIViewController, CodeInstantiableController {
    let store: Store<AppState, AppAction>
    let viewStore: ViewStore<ViewState, AppAction>
    let tableView: TableView<ToDo, ToDoTableViewCell>
    
    struct ViewState: Equatable {
        init(state: AppState) {
            // TODO: initialize!
            print("Initialize viewState!")
        }
    }
    
    init(_ store: Store<AppState,AppAction>) {
        self.store = store
        self.viewStore = ViewStore(store.scope(state: ViewState.init))
        self.tableView = TableView<ToDo, ToDoTableViewCell>(rowHeight: 16)
        super.init(nibName: nil, bundle: nil)
        self.setUpViews()
    }
    
    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
    
    internal func setUpViews() {
        let view = View()
        view.constrainToParent(self.view)
        view.backgroundColor = .red
    }
    
    override func viewDidLoad() {
        print("View Did Load! ")
        
    }
    

}

