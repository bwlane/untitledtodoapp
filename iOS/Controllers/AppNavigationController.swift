//
//  AppNavigationController.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

import UIKit
import ComposableArchitecture

class AppNavigationController {
    let navigationController: UINavigationController
    
    init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
}

