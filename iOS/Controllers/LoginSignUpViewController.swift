//
//  LoginSignupViewController.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/5/21.
//

import UIKit
import ComposableArchitecture

class LoginSignUpViewController: UIViewController {
    let store: Store<LoginSignUpState,LoginSignUpAction>
    
    init(store: Store<LoginSignUpState,LoginSignUpAction>) {
        self.store = store
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
