//
//  ContentView.swift
//  Shared
//
//  Created by Brian Lane on 5/19/21.
//

import SwiftUI
import ComposableArchitecture

struct AppView: View {
    let store: Store<AppState,AppAction>
    @ObservedObject var viewStore: ViewStore<ViewState,AppAction>

    public init(_ store: Store<AppState,AppAction>) {
        self.store = store
        self.viewStore = ViewStore(self.store.scope(state: ViewState.init(state:)))
    }
    
    struct ViewState: Equatable {
        let editMode: EditMode
        let filter: Filter
        let isClearCompletedButtonDisabled: Bool
        
        init(state: AppState) {
            self.editMode = state.editMode
            self.filter = state.filter
            self.isClearCompletedButtonDisabled = !state.todos.contains(where: \.isComplete)
        }
    }
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                Picker("Filter", selection: self.viewStore.binding(get: \.filter, send: AppAction.filterPicked).animation()
                ) {
                    ForEach(Filter.allCases, id: \.self) { filter in
                        Text(filter.rawValue).tag(filter)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.horizontal)
                
                List {
                    ForEachStore(
                        self.store.scope(state: \.filteredToDos, action: AppAction.todo(id:action:)), content: ToDoView.init(store:)
                    )
                    .onDelete { self.viewStore.send(.delete($0)) }
                    .onMove { self.viewStore.send(.move($0, $1)) }
                }
            }
            .navigationBarTitle("Todos")
            .navigationBarItems(
                trailing: HStack(spacing: 20) {
                    EditButton()
                    Button("Clear Completed") {
                        self.viewStore.send(.clearCompletedButtonTapped, animation: .default)
                        
                    }
                    .disabled(self.viewStore.isClearCompletedButtonDisabled)
                    Button("Add Todo") {
                        self.viewStore.send(.addToDoButtonTapped, animation: .default)
                    }
                }
            ).environment(
                \.editMode,
                self.viewStore.binding(get: \.editMode, send: AppAction.editModeChanged)
            )
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView(
            Store(
                initialState: AppState(todos: .mock),
                reducer: appReducer,
                environment: AppEnvironment(
                    mainQueue: .main,
                    uuid: UUID.init
                )
            )
        )
    }
}


