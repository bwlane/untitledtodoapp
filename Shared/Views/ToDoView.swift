//
//  ToDoView.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/1/21.
//

import SwiftUI
import ComposableArchitecture

struct ToDoView: View {
    let store: Store<ToDo, ToDoAction>
    
    var body: some View {
        WithViewStore(self.store) { viewStore in
            HStack {
                Button(action: { viewStore.send(.checkBoxToggled) }) {
                    Image(systemName: viewStore.isComplete ? "checkmark.square" : "square" )
                }
                .buttonStyle(PlainButtonStyle())
                
                TextField(
                    "Untitled Todo",
                    text: viewStore.binding(get: \.description, send: ToDoAction.textFieldChanged)
                )
            }.foregroundColor(viewStore.isComplete ? .gray : nil)
        }
    }
}
