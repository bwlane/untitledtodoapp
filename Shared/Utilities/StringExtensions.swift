//
//  StringExtensions.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

import Foundation


extension String {
    static func timestamp() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = formatter.string(from: now)
        return dateString
    }
}
