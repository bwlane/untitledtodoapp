//
//  ToDoDomain.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/1/21.
//

import Foundation
import ComposableArchitecture

public struct ToDoState {} // We don't need to use this because the state is equivalent to the thing itself

struct ToDo: Equatable, Identifiable {
    var description = ""
    let id: UUID
    var isComplete = false
}

extension IdentifiedArray where ID == UUID, Element == ToDo {
    mutating func sortCompleted() {
        self = IdentifiedArray(
            self.enumerated()
                .sorted(by: { lhs, rhs in
                    (rhs.element.isComplete && !lhs.element.isComplete) || lhs.offset < rhs.offset
                }).map(\.element)
        )
    }
}

extension IdentifiedArray where ID == UUID, Element == ToDo {
  static let mock: Self = [
    ToDo(
      description: "Check Mail",
      id: UUID(uuidString: "DEADBEEF-DEAD-BEEF-DEAD-BEEDDEADBEEF")!,
      isComplete: false
    ),
    ToDo(
      description: "Buy Milk",
      id: UUID(uuidString: "CAFEBEEF-CAFE-BEEF-CAFE-BEEFCAFEBEEF")!,
      isComplete: false
    ),
    ToDo(
      description: "Call Mom",
      id: UUID(uuidString: "D00DCAFE-D00D-CAFE-D00D-CAFED00DCAFE")!,
      isComplete: true
    ),
  ]
}

public enum ToDoAction: Equatable {
    case checkBoxToggled
    case textFieldChanged(String)
}

struct ToDoEnvironment {}
