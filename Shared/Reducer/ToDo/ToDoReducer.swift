//
//  ToDoReducer.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/1/21.
//

import Foundation
import ComposableArchitecture

let toDoReducer = Reducer<ToDo, ToDoAction, ToDoEnvironment> { todo, action, _ in
    switch action {
    case .checkBoxToggled:
        todo.isComplete.toggle()
        return .none
    case let .textFieldChanged(description):
        todo.description = description
        return .none
    }
}
