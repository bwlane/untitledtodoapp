//
//  AppReducer.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/1/21.
//

import Foundation
import ComposableArchitecture

let appReducer = Reducer<AppState, AppAction, AppEnvironment>.combine(
    toDoReducer.forEach(
        state: \.todos,
        action: /AppAction.todo(id:action:),
        environment: { _ in ToDoEnvironment() }
    ), Reducer { state, action, environment in
    switch action {
    
    case .addToDoButtonTapped:
        state.todos.insert(ToDo(id: environment.uuid()), at: 0)
        return .none
        
    case .clearCompletedButtonTapped:
        state.todos.removeAll(where: \.isComplete)
        return .none
        
    case let .delete(indexSet):
        state.todos.remove(atOffsets: indexSet)
        return .none
    
    case let .editModeChanged(editMode):
        state.editMode = editMode
        return .none
        
    case let .filterPicked(filter):
        state.filter = filter
        return .none
    
    case let .move(source, destination):
        state.todos.move(fromOffsets: source, toOffset: destination)
        return Effect(value: AppAction.sortCompletedToDos)
            .delay(for: .milliseconds(100), scheduler: environment.mainQueue)
            .eraseToEffect()
        
    case .sortCompletedToDos:
        state.todos.sortCompleted()
        return .none
        
    case .todo(id: _, action: .checkBoxToggled):
        struct ToDoCompletionId: Hashable {}
        return Effect(value: AppAction.sortCompletedToDos)
            .debounce(
                id: ToDoCompletionId(),
                for: 1,
                scheduler: environment.mainQueue.animation()
            )
        
    case .todo:
        return .none
    }
}
)
