//
//  Domain.swift
//  UntitledToDoApp (iOS)
//
//  Created by Brian Lane on 6/1/21.
//

import SwiftUI
import ComposableArchitecture

public enum Filter: LocalizedStringKey, CaseIterable, Hashable {
    case all = "All"
    case active = "Active"
    case completed = "Completed"
}

public struct AppState: Equatable {
    var editMode: EditMode = .inactive
    var filter: Filter = .all
    var todos: IdentifiedArrayOf<ToDo> = []
    
    var filteredToDos: IdentifiedArrayOf<ToDo> {
        switch filter {
        case .active: return self.todos.filter { !$0.isComplete }
        case .all: return self.todos
        case .completed: return self.todos.filter(\.isComplete)
        }
    }
}


public enum AppAction: Equatable {
    // TODO
    case addToDoButtonTapped
    case clearCompletedButtonTapped
    case delete(IndexSet)
    case editModeChanged(EditMode)
    case filterPicked(Filter)
    case move(IndexSet, Int)
    case sortCompletedToDos
    case todo(id: UUID, action: ToDoAction)
}

public struct AppEnvironment {
    var mainQueue: AnySchedulerOf<DispatchQueue>
    var uuid: () -> UUID
}
