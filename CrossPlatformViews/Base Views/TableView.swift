//
//  TableView.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

#if os(iOS)
import UIKit
#elseif os(macOS)
import Cocoa
#endif

// MARK: - View

#if os(iOS)
protocol Reusable {
    static func reuseIdentifier() -> String
}

class TableView<A, B: Reusable>: UITableView, UITableViewDelegate, UITableViewDataSource {
    private var _collection: [A] = []
    var collection: [A] {
        get {
            return self._collection
        }
        set {
            self._collection = newValue
            self.reloadData()
        }
    }
    var updateCellWithContent: ((inout B, A) -> Void)?
    var didSelectContent: ((Int, A) -> Void)?
    
    private let _rowHeight: CGFloat
    
    init(rowHeight: CGFloat) {
        self._rowHeight = rowHeight
        super.init(frame: .zero, style: .plain)
        self.dataSource = self
        self.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._collection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: B.reuseIdentifier(), for: indexPath) as? B else {
            return UITableViewCell()
        }
        
        let content = self._collection[indexPath.row]
        self.updateCellWithContent?(&cell, content)
        
        if cell is UITableViewCell {
            return cell as! UITableViewCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self._rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectContent?(indexPath.row, self._collection[indexPath.row])
    }
}
#elseif os(macOS)
class TableView<A>: NSTableView, NSTableViewDelegate, NSTableViewDataSource {
    private var _collection: [A] = []
    var collection: [A] {
        get {
            return self._collection
        }
        set {
            self._collection = newValue
            self.reloadData()
        }
    }
    var didSelectContent: ((Int, A) -> Void)?
    var viewForContent: ((A) -> View?)?
    
    private let _rowHeight: CGFloat
    
    init(rowHeight: CGFloat) {
        self._rowHeight = rowHeight
        super.init(frame: .zero)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        return self.viewForContent?(self._collection[row])
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return self._rowHeight
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        guard let tableView = notification.object as? TableView else {
            return
        }
        let selectedRow = tableView.selectedRow
        guard selectedRow >= 0, selectedRow < tableView.collection.count else { return }
        self.didSelectContent?(selectedRow, self._collection[selectedRow])
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return self._collection.count
    }
    
    func hasOverflowCells() -> Bool {
        guard self._collection.count > 0, let scrollViewHeight = self.enclosingScrollView?.documentVisibleRect.size.height else { return false }
        let profileCellHeight = self.frameOfCell(atColumn: 0, row: 0).size.height
        let count = CGFloat(self._collection.count)
        return profileCellHeight * count > scrollViewHeight
    }

}
#endif
