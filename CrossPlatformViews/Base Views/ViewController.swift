//
//  ViewController.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

#if os(iOS)
import UIKit
#elseif os(macOS)
import Cocoa
#endif

// MARK: - View

#if os(iOS)
typealias ViewController = UIViewController
#elseif os(macOS)
typealias ViewController = NSViewController
#endif
