//
//  View.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

#if os(macOS)
import Cocoa
#elseif os(iOS)
import UIKit
#endif

// MARK: - View

#if os(macOS)
typealias View = NSView
#elseif os(iOS)
typealias View = UIView
#endif


// MARK: - Light/Dark Mode Extension
extension View {
    internal func appearance() -> Appearance {
        #if os(macOS)
        return Appearance.fromNSAppearance(self.effectiveAppearance)
        #elseif os(iOS)
        return Appearance.fromTraitCollection(self.traitCollection)
        #endif
    }
}


extension View {
    func setStandard() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func constrainChildToSelf(_ child: View) {
        self.addSubview(child)
        child.setStandard()
        child.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        child.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        child.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        child.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func constrainToParent(_ parent: View) {
        parent.addSubview(self)
        self.setStandard()
        self.leadingAnchor.constraint(equalTo: parent.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: parent.trailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: parent.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: parent.bottomAnchor).isActive = true
    }
}
