//
//  CodeInstantiableController.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

import Foundation

protocol CodeInstantiableController {
    func setUpViews()
}
