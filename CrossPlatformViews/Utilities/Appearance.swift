//
//  Appearance.swift
//  UntitledToDoApp
//
//  Created by Brian Lane on 6/4/21.
//

#if os(iOS)
import UIKit
#elseif os(macOS)
import Cocoa
#endif

enum Appearance {
    case light
    case dark
    
    #if os(macOS)
    static func fromNSAppearance(_ nsAppearance: NSAppearance?) -> Self {
        guard let nsAppearance = nsAppearance else {
            return .light
        }
        switch nsAppearance.name {
        case .darkAqua:
            return .dark
        default:
            return .light
        }
    }
    #elseif os(iOS)
    static func fromTraitCollection(_ traitColleciton: UITraitCollection) -> Self {
        switch traitColleciton.userInterfaceStyle {
        case .unspecified, .light:
            return .light
        case .dark:
            return .dark
        @unknown default:
            fatalError("There is an uncovered case in iOS UITraitCollection")
        }
    }
    #endif
}
