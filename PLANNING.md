This is a basic ToDo App, using the Composable Architecture.

A user should be able to:

1. Open the app
2. Log in / Authenticate
3. See a list of their ToDo projects
4. Create a ToDo Project
5. Open a window of their ToDo projects
6. Add/delete ToDos
7. Add/delete ToDo Projects 